FROM node:12-alpine

# Crea el directorio de la app
WORKDIR /usr/src/app

# Copiar y instalar las dependiencias
COPY src/package*.json ./
RUN npm install

# Reto 1 - Ejecutar con usuario diferente de root
USER node

# Copia aplicacion al contenedor
COPY src .

EXPOSE 3000
CMD [ "node", "index.js" ]