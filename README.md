# Soluciones Reto-DevOps CLM
![CLM Consoltores](./src/img/clm.png)

### Reto 1. Dockerize la aplicación

1. La imagen se construyó sobre la versión alpine para hacerla pequeña.
2. Al ejecutar la app el usuario por defecto es **(node)**.
3. Para ejecutar la aplicación debes tener **(Docker)**, mas información sobre instalación de Docker [aquí](https://docs.docker.com/engine/install/).


```bash
docker build . -t reto-devops:1.0.0
docker run --rm -p 3000:3000 reto-devops:1.0.0
```

### Reto 2. Docker Compose

1. Nginx esta configurado como proxy reverso.
2. El endpoint /private esta asegurado con auth_basic, la password es admin.
3. Se habilito la redireccion y https con un certificado auto firmado para [localhost](https://localhost/).
4. El Certificado por ser auto firmado va ha requerir la confirmación del navegador **Continuar a localhost (no seguro)**.

```bash
docker-compose up --build -d
```

### Reto 3. Probar la aplicación en cualquier sistema CI/CD

1. Se realizo con gitlab-ci, usando un gitlab-runner de tipo Shell, puede ver el historial de ejecuciones en el menú [CI/CD - Pipelines](https://gitlab.com/restrepoduran1/reto-devops/-/pipelines).

### Reto 4. Deploy en kubernetes

1. Utilice la herramienta Microk8s.
2. Los manifests se encuentran en el directorio **kubernetes** del proyecto.
3. En el menú [Deployments - Environments](https://gitlab.com/restrepoduran1/reto-devops/-/environments) podrán ver los sitios en vivo donde se ha realizado el deploy.
4. Añadí un Autoscaler de máximo 10 replicas si la CPU llega ha 80%.

### Reto 5. Construir Chart en helm y manejar trafico http(s)

1. Se creo un Chart Helm de nombre reto-5, esta dentro del directorio helm.
2. Se agrego un template de ingress al chart para solicitar auth_basic.
3. Agrege el annotations al ingress para forzar la redireccion.
4. En el menú [Deployments - Environments](https://gitlab.com/restrepoduran1/reto-devops/-/environments) podrán ver los sitios en vivo donde se ha realizado el deploy.

Alcance ha realizar hasta el reto 5, gracias por leer esta documentacion.
